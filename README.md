requirement: vim8

clone into user home folder
```
apt install vim-nox
cd
git clone https://gitlab.com/bevinhex/vim.git
mv ./vim ./.vim
ln -s ./.vim/.vimrc ./.vimrc
```
open vim, type `:PlugInstall` to let Vundle install dependencies

after finish, run `post-install.sh`, probably you also need to install golang if not installed , if not needed, you can comment out golang related parameter from post-install.sh , then run it, what it does is compile YouCompleteMe plugin for autocompletion

once done, you are ready to go
