#!/bin/bash
#compile YouCompleteMe dependencies
echo 'for details please check https://vimawesome.com/plugin/youcompleteme'
sudo apt install build-essential cmake python3-dev xsel
sudo pip3 install cmake
cd ~/.vim/plugged/YouCompleteMe
python3 install.py --clang-completer --cs-completer

os=$(grep '^ID' /etc/os-release)
if [ $os == "" ];then
  echo "Unable to detect OS"
  exit 1
fi
os=${os#*=}
if [ $os == "debian" ];then
  sudo apt install fonts-powerline
fi
