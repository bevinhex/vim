"__________________________VIM-Plug plugin manager____BEGIN____________________
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

silent! py3 pass

" polyglot setup
"____________________________________________________________________________
let g:polyglot_disabled = ['autoindent'] "don't let polyglot mess with tab width
"____________________________________________________________________________

set nocompatible              		" be iMproved, required
filetype off

call plug#begin()

Plug 'flazz/vim-colorschemes'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'ctrlpvim/ctrlp.vim'

Plug 'ycm-core/YouCompleteMe'

Plug 'sheerun/vim-polyglot'   "syntax highlighting for all languages

Plug 'christoomey/vim-system-copy'

Plug 'tomtom/tcomment_vim'

Plug 'scrooloose/nerdtree'
Plug 'nopik/vim-nerdtree-direnter'
Plug 'jistr/vim-nerdtree-tabs'

Plug 'justinmk/vim-sneak'
"Plug 'maxmellon/vim-jsx-pretty' "pretify jsx, complement polygot

call plug#end()            		" required
filetype plugin indent on    		" required

"____________________________________________________________________________
" tcomment setup
let g:tcomment#options = { 'col': 1, 'whitespace' : 'no' }
"____________________________________________________________________________
"nerdtree setup
nnoremap <C-n> :NERDTreeTabsToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
let NERDTreeMapOpenInTab='<ENTER>'
let g:nerdtree_tabs_meaningful_tab_names=0
let g:nerdtree_tabs_focus_on_files=1
set mouse=a
let g:NERDTreeMouseMode=2
autocmd VimEnter * call NERDTreeAddKeyMap({ 'key': '<2-LeftMouse>', 'scope': "FileNode", 'callback': "OpenInTab", 'override':1 })
    function! OpenInTab(node)
        call a:node.activate({'reuse': 'all', 'where': 't','keepopen':1})
    endfunction
"____________________________________________________________________________
" airline plugin setup

let g:airline#extensions#tabline#enabled=1
let g:airline_powerline_fonts=1

let g:airline#extensions#tabline#formatter='unique_tail'

let g:airline#extensions#tabline#fnamemod=':t'       "only show filename

nnoremap <tab> :tabnext<CR>
nnoremap <S-tab> :tabprevious<CR>
"____________________________________________________________________________
"ctrlp plugin setup
"let ctrlp open new files in new tab, instead of having to press Ctrl-t
let g:ctrlp_prompt_mappings = {
  \ 'AcceptSelection("e")': ['<2-LeftMouse>'],
  \ 'AcceptSelection("t")': ['<cr>']
  \}
let g:ctrlp_custom_ignore = 'node_modules\|git'
let g:ctrlp_user_command = [
    \ '.git', 'cd %s && git ls-files . -co --exclude-standard',
    \ 'find %s -type f'
    \ ]
"____________________________________________________________________________
"change swap directory to tmp/swap, so that it won't create it on current directory of files , causing recompile for some stuff
set dir=$HOME/.vim/tmp/swap
if !isdirectory(&dir) | call mkdir(&dir,'p',0700) | endif

set number relativenumber "line number
"linenumber colors, cannot set those becaue there is no left margin for the
"code at the moment, looks ugly
"highlight LineNr ctermfg=black
"highlight LineNr ctermbg=green

"indent wrapped lines
set breakindent
set showbreak=>>

"set tabstop
set autoindent
set expandtab				      "tabs are spaces
set tabstop=2
set softtabstop=2
set shiftwidth=2

"don't let ftplugin mess with tabstops
let g:python_recommended_style = 0

set showcmd				        "show command in bottom bar

set cursorline				    "highlight current line

set wildmenu				      "visual autocomplete for command menu

set completeopt-=preview  "disable annoying scratch buffer

syntax on				          "syntax highlight
colorscheme gruvbox			  "color theme
set background=dark       "dark mode of the theme

set lazyredraw				    "redraw only when we need to

set showmatch				      "highlight matching [{()}]

set hlsearch				      "highlight matches

nnoremap <leader><space> :nohlsearch<CR>  "turn off search when space key is pressed

"folding
set foldenable
set foldlevelstart=10     "open most folds by default
set foldnestmax=10        "10 nested fold max to save cpu
nnoremap <space> za       "space open/closes folds
set foldmethod=indent     "fold based on indent level

"____________________________________________________________________________
" Load custom functions
source ~/.vim/predefined_texts.vim
nmap <C-l> :call InsertCommentedLine()<CR>
